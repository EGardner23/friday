__author__ = "Elizabeth Gardner"
__date__ = "11 October 2020"
# CSC 355: Open Source Development
# white_background_black_circle.py
"""Produce an image using a mask that has a white background with a black circle in the middle.

Answer to Piazza post on creating simple masks.
"""
import numpy as np

from PIL import Image, ImageDraw


def main():
    """Produce the composite image."""
    print("white background black circle")

    picture = Image.open(r"C:\Users\eliza\Pictures\ProfilePic.jpg")

    width, height = picture.size

    # size of bounding box for ellipse: I got the idea for the bounding box from
    # https://stackoverflow.com/questions/4789894/python-pil-how-to-draw-an-ellipse-in-the-middle-of-an-image
    box_width = 75
    box_height = 75

    bbox = (width/2 - box_width/2, height/2 - box_height/2, width/2 + box_width/2, height/2
            + box_height/2)

    # white_image = np.full((height, width), 255, dtype=np.uint8)
    black_circle = Image.fromarray(np.zeros((height, width, 3)), "RGB")

    mask = Image.new("L", picture.size, 255)
    draw = ImageDraw.Draw(mask)
    draw.ellipse(bbox, fill=0)
    photo = Image.composite(picture, black_circle, mask)

    photo.save("white_background_black_circle.png")
    photo.show()


if __name__ == "__main__":
    main()
