__author__ = "Elizabeth Gardner"
__date__ = "13 October 2020"
# CSC 355: Open Source Development
# toning.py
"""Produce an image in a user-selected rainbow tone."""
from PIL import Image


# I got the idea and the basic methods for this project from
# https://www.blog.pythonlibrary.org/2017/10/11/convert-a-photo-to-black-and-white-in-python/
# which I found when I was looking up ways to convert a color photo to black and white.

def make_sepia_palette(color):
    """Make a new color palette for the toned image based on the chosen color."""
    palette = []
    r, g, b = color
    for i in range(255):
        palette.extend((r * i // 255, g * i // 255, b * i // 255))
    return palette


def create_sepia(input_image_path):
    """Produce an toned image in the color selected by the user.

    Parameter: input_image_path: str
        the path to the image to be toned
    """
    color_response = input("Select a color.\n")

    # I thought that only toning pictures in sepia (brown) was a bit boring, so I decided to give a few more
    # exciting color options that the user can choose from.
    # I looked up X11 colors to find the standard RGB values of the colors of the rainbow.

    if color_response == "red":
        color = (255, 0, 0)
    elif color_response == "orange":
        color = (255, 165, 0)
    elif color_response == "yellow":
        color = (255, 255, 0)
    elif color_response == "green":
        color = (0, 255, 0)
    elif color_response == "blue":
        color = (0, 0, 255)
    elif color_response == "purple":
        color = (160, 32, 240)
    else:
        color = (255, 255, 255)

    sepia = make_sepia_palette(color)

    color_image = Image.open(input_image_path)

    bw = color_image.convert('L')

    # add the sepia toning
    bw.putpalette(sepia)

    sepia_image = bw.convert('RGB')

    return sepia_image


def main():
    """Create, save, and show the toned image."""
    print("Produce a rainbow-toned image.")
    picture = create_sepia(r"C:\Users\eliza\Pictures\ProfilePic.jpg")
    picture.save("toning.png")
    picture.show()


if __name__ == "__main__":
    main()
