__author__ = "Elizabeth Gardner"
__date__ = "9 October 2020"
# CSC 355: Open Source Development
# friday.py
"""Produce an image with a black striped mask on the leftmost and rightmost quarter of the image."""
import numpy as np

from PIL import Image


def main():
    """Produce a composite image."""

    # I made this program last Friday. I believe mine is almost exactly like the one you showed us.

    print("Composite")

    picture = Image.open(r"C:\Users\eliza\Pictures\flowers-wall.jpg")
    picture = picture.reduce(3)

    width, height = picture.size
    print(f"photo measures {width} by {height} pixels")

    black_image = Image.fromarray(np.zeros((height, width, 3)), "RGB")
    width, height = black_image.size
    print(f"black_image measures {width} by {height} pixels")

    mask = np.full((height, width), 255, dtype=np.uint8)

    # the mask on the leftmost quarter of the image
    # the black stripes are 2 pixels apart
    for i in range(0, width//4, 2):
        mask[:, i] = 0

    # the mask on the rightmost quarter of the image
    # the black stripes are 4 pixels apart
    for i in range(3 * width//4, width, 4):
        mask[:, i] = 0

    # produces solid black bars on the left and right sides of the image instead of stripes
    # mask[:, 0:(width//4)] = 0
    # mask[:, (3 * width//4):width] = 0

    mask_image = Image.fromarray(mask, "L")
    width, height = mask_image.size
    print(f"mask_image measures {width} by {height} pixels")

    photo = Image.composite(picture, black_image, mask_image)

    photo.show()


if __name__ == "__main__":
    main()
