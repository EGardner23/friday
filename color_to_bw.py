__author__ = "Elizabeth Gardner"
__date__ = "12 October 2020"
# CSC 355: Open Source Development
# color_to_bw.py
"""Produce a black and white/grayscale version of a color photo."""
# I had some trouble getting my algorithm to work at first: since a JPEG image is not iterable, I tried converting
# the image to a numpy array to be used in the for loops, but I finally found that all that was needed was the width
# and height of the image. Therefore, I no longer need numpy, though I have left the pieces of my array attempt in
# comments below.
# import numpy as np

from PIL import Image


def main():
    """Produce a grayscale version of a color photo by taking the average of each pixel's r, g, and b values."""
    print("color to black and white")

    picture = Image.open(r"C:\Users\eliza\Pictures\ProfilePic.jpg")

    width, height = picture.size

    # I initially used the convert method to make a black and white version of the picture.
    # picture_gray = picture.convert("L")

    # Since I discovered that the same algorithm can be used to change a color image to black and white
    # and reverse the colors of a color image, I decided to make both an option in this program.
    response = input("Black and white or reversed colors?\n")

    if response == "black and white":
        for x in range(0, width):
            for y in range(0, height):
                coordinate = (x, y)
                # print(picture.getpixel(coordinate))
                r, g, b = picture.getpixel(coordinate)
                average = (r + g + b) // 3
                r = average
                g = average
                b = average
                picture.putpixel((x, y), (r, g, b))

    elif response == "reversed colors":
        for x in range(0, width):
            for y in range(0, height):
                coordinate = (x, y)
                r, g, b = picture.getpixel(coordinate)
                r = 255 - r
                g = 255 - g
                b = 255 - b
                picture.putpixel((x, y), (r, g, b))

    # array = np.asarray(picture_gray)
    # pic_array = array.copy()

    # height, width, depth = pic_array.shape

    # I started this project by looking at the example from class on darkening a rectangle in the middle of the image.
    # slice = pic_array[(height//4):(3*height//4), (width//4):(3*width//4)]
    # pic_array[(height//4):(3*height//4), (width//4):(3*width//4)] = slice // 2

    # pic_image = Image.fromarray(pic_array)

    # picture_gray.show()

    # pic_array = Image.fromarray(pic_array)
    picture.save("color_to_bw.png")
    picture.show()


if __name__ == "__main__":
    main()
