__author__ = "Elizabeth Gardner"
__date__ = "15 October 2020"
# CSC 355: Open Source Development
# painting.py
"""Make a photographic image look like a painting or drawing."""
from PIL import Image, ImageOps, ImageFilter


def main():
    """Alter an image so that it looks like a painting instead of a photograph."""
    print("make a photo look like a painting")

    picture = Image.open(r"C:\Users\eliza\Pictures\landscape.jpg")

    original_picture = picture.reduce(2)

    # I have been trying out a couple of different methods of modifying an image to make it look like it was
    # drawn or painted.

    # painting = original_picture.convert("1")

    # I got the idea to use ImageFilter methods in class today (Thursday).
    painting = original_picture.filter(ImageFilter.CONTOUR)

    # Add a frame to make it look more like a piece of art.
    framed_painting = ImageOps.expand(painting, border=10, fill=0)

    framed_painting.save("painting.png")
    framed_painting.show()


if __name__ == "__main__":
    main()
