__author__ = "Elizabeth Gardner"
__date__ = "14 October 2020"
# CSC 355: Open Source Development
# colorize.py
"""Produce a recolored image by converting a picture to grayscale then 
replacing the blacks and whites with new given colors.
"""
from PIL import Image
# I saw the colorize method while looking through the documentation for the ImageOps module.
# This is just a simple project to see how colorize works, and it produced an interesting image.
from PIL import ImageOps


def main():
    """Produce a colorized image."""
    print("colorize")

    picture = Image.open(r"C:\Users\eliza\Pictures\forget-me-not.jpg")

    # convert the picture to black and white
    bw_picture = picture.convert("L")

    # replace the blacks with blue and the whites with red
    colorized_picture = ImageOps.colorize(bw_picture, (0, 0, 255), (255, 0, 0))

    colorized_picture.save("colorize.png")
    colorized_picture.show()


if __name__ == "__main__":
    main()
