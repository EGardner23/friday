__author__ = "Elizabeth Gardner"
__date__ = "14 October 2020"
# CSC 355: Open Source Development
# split.py
"""Experiment with split."""
import numpy as np

from PIL import Image, ImageOps


def main():
    """Produce an image by separating the red, green, and blue bands of an image, adding color to each of the three
    resulting images, and recombining them."""
    print("split")

    picture = Image.open(r"C:\Users\eliza\Pictures\landscape.jpg")

    # This is just a little project to see how to use split.
    # https://www.geeksforgeeks.org/python-pil-image-split-method/
    # was a helpful source for this project

    # split() method
    # this will split the image in individual bands
    # and return a tuple
    # split_pic = Image.Image.split(picture)

    # separate the red, green, and blue components of an image
    r, g, b = picture.split()

    # convert the three tuples to images, then add some color to each
    # I experimented with multiple different colors, but I think this simple one
    # that replaces the blue with more red produces a nice picture.
    r_array = np.asarray(r)
    r_pic = Image.fromarray(r_array, "L")
    r_color = ImageOps.colorize(r_pic, (255, 0, 0), (255, 255, 255))

    g_array = np.asarray(g)
    g_pic = Image.fromarray(g_array, "L")
    g_color = ImageOps.colorize(g_pic, (0, 255, 0), (255, 255, 255))

    b_array = np.asarray(b)
    b_pic = Image.fromarray(b_array, "L")
    b_color = ImageOps.colorize(b_pic, (255, 0, 0), (255, 255, 255))

    # From geeksforgeeks.org; used to show each band separately.
    # showing each band
    # split_pic[0].show()
    # split_pic[1].show()
    # split_pic[2].show()

    # To recombine the unaltered bands and reproduce the original picture.
    # merge_pic = Image.merge("RGB", (r, b, g))
    # merge_pic.show()

    rg = Image.blend(r_color, g_color, 0.5)
    rgb = Image.blend(rg, b_color, 0.33)

    rgb.show()


if __name__ == "__main__":
    main()
