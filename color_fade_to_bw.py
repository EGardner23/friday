__author__ = "Elizabeth Gardner"
__date__ = "16 October 2020"
# CSC 355: Open Source Development
# color_fade_to_bw.py
"""Attempt to produce an image that is in full color on the left side, then transitions to become fully in
black and white on the right side.
"""

# I have tried multiple methods to make an image fade from color to black and white after hearing it suggested in class
# but have not yet gotten it worked out.

# import numpy as np

from PIL import Image


def main():
    """Take a color picture and alter the colors from the left side to the right side of the image."""
    print("color fade to black and white")

    color_picture = Image.open(r"C:\Users\eliza\Pictures\tulips.jpg")
    # color_picture = color_picture.convert("RGB")
    # print(color_picture.size)

    # bw_picture = color_picture.convert("L")
    # bw_rgb_picture = bw_picture.convert("RGB")

    # mask = Image.linear_gradient("RGB")
    # new_mask = mask.rotate(90)

    # I encountered many problems with either a picture or a mask being in the wrong mode to be combined
    # even though I converted them to the same mode above.

    # pic_image = Image.blend(color_picture, bw_rgb_picture, 0.5)

    # pic_image = Image.composite(color_picture, bw_rgb_picture, mask)

    # pic_image = Image.alpha_composite(color_picture, mask)

    # I used the same basic algorithm that I used to convert a color photo to black and white to try to make this same
    # transition more slowly across the image, though I haven't been able to find any formula to fade the colors.
    width, height = color_picture.size

    for x in range(0, width):
        for y in range(0, height):
            coordinate = (x, y)
            r, g, b = color_picture.getpixel(coordinate)
            average = (r + g + b) // 3
            r_inc = ((r - average) // width)
            g_inc = ((g - average) // width)
            b_inc = ((b - average) // width)
            r = r - (x * r_inc)
            g = g - (x * g_inc)
            b = b - (x * b_inc)
            color_picture.putpixel((x, y), (r, g, b))

    # an attempt to make each pixel's color based on a weighted average of its RGB values
    # (so, for example, a red pixel becomes a more muted shade of red)
    # for x in range(width // 3, 2 * (width // 3)):
    #     for y in range(0, height):
    #         coordinate = (x, y)
    #         r, g, b = color_picture.getpixel(coordinate)
    #         r_band, g_band, b_band = color_picture.split()
    #         r = np.average(color_picture, axis=x, weights=np.asarray(r_band))
    #         g = np.average(color_picture, axis=x, weights=np.asarray(g_band))
    #         b = np.average(color_picture, axis=x, weights=np.asarray(b_band))
    #         color_picture.putpixel((x, y), (r, g, b))

    color_picture.show()

    # pic_image.show()


if __name__ == "__main__":
    main()
