__author__ = "Elizabeth Gardner"
__date__ = "11 October 2020"
# CSC 355: Open Source Development
# black_background_white_circle.py
"""Produce an image using a mask that has a black background with a white circle in the middle.

Answer to Piazza post on creating simple masks.
"""
import numpy as np

from PIL import Image, ImageDraw


def main():
    """Produce the composite image."""
    print("black background white circle")

    picture = Image.open(r"C:\Users\eliza\Pictures\ProfilePic.jpg")

    width, height = picture.size

    # size of bounding box for ellipse: I got the idea for the bounding box from
    # https://stackoverflow.com/questions/4789894/python-pil-how-to-draw-an-ellipse-in-the-middle-of-an-image
    box_width = 75
    box_height = 75

    bbox = (width/2 - box_width/2, height/2 - box_height/2, width/2 + box_width/2, height/2
            + box_height/2)

    # black background
    black_image = Image.fromarray(np.zeros((height, width, 3)), "RGB")

    # white circle
    mask = Image.new("L", picture.size, 0)
    draw = ImageDraw.Draw(mask)
    draw.ellipse(bbox, fill=255)
    photo = Image.composite(picture, black_image, mask)

    photo.save("black_background_white_circle.png")
    photo.show()


if __name__ == "__main__":
    main()
